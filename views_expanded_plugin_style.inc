<?php

/**
 * @file
 * Provide an accordion style plugin for Views. This file is autoloaded by views.
 */

/**
 * Implementation of views_plugin_style().
 */
class views_expanded_plugin_style extends views_plugin_style {

  protected $entity_type;

  public function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options);

    // Initialize the entity-type used.
    $table_data = views_fetch_data($this->view->base_table);
    $this->entity_type = $table_data['table']['entity type'];
  }
  
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['view_mode'] = array('default' => 'default');
    $options['content_height'] = array('default' => '');
    $options['attach_css'] = array('default' => 1);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $entity_info = entity_get_info($this->entity_type);
    $options = array();
    if (!empty($entity_info['view modes'])) {
      foreach ($entity_info['view modes'] as $mode => $settings) {
        $options[$mode] = $settings['label'];
      }
    }

    if (count($options) > 1) {
      $form['view_mode'] = array(
        '#type' => 'select',
        '#options' => $options,
        '#title' => t('View mode'),
        '#default_value' => $this->options['view_mode'],
        '#description' => "This view mode will be used to generate expanded content items.",
      );
    }
    else {
      $form['view_mode_info'] = array(
        '#type' => 'item',
        '#title' => t('View mode'),
        '#description' => t('Only one view mode is available for this entity type.'),
        '#markup' => $options ? current($options) : t('Default'),
      );
      $form['view_mode'] = array(
        '#type' => 'value',
        '#value' => $options ? key($options) : 'default',
      );
    }
    
    $form['content_height'] = array(
      '#type' => 'textfield',
      '#title' => 'Set height',
      '#default_value' => $this->options['content_height'],
      '#field_suffix' => 'px',
      '#size' => 10,
      '#description' => "<strong>Not usable yet!</strong> You can force height property on each expanded item. Leave empty if you don't want to use this feature.",
    );
    
    $form['attach_css'] = array(
      '#type' => 'checkbox',
      '#title' => t('Attach default CSS styles'),
      '#default_value' => $this->options['attach_css'],
      '#description' => t("Provide default CSS styles by this module."),
    );
  }
}
