<?php
/**
 * @file
 * Displays the the view-rows and does something more which should be
 * documented here
 */
?>
<div class="views-expanded views-expanded-nojs">
  <ul class="views-expanded-list">
    <?php foreach ($rows as $id => $row): ?>
      <li class="views-expanded-item views-expanded-item-<?php print $id; ?>">
        <div class="views-expanded-row">
          <?php print $row; ?>
        </div>
        <div class="views-expanded-content">
          <div class="inner">
            <?php print render($views_expanded_items[$id]); ?>
          </div>
        </div>
      </li>
    <?php endforeach; ?>
  </ul>
</div>