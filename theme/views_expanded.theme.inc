<?php

/**
 * Template function for views_expanded style plugin.
 *
 * @param array $vars
 *  Array of template variables.
 *
 */
function template_preprocess_views_view_expanded(&$vars) {
  $handler = $vars['view']->style_plugin;

  if ($handler->options['attach_css']) {
    drupal_add_css(drupal_get_path('module', 'views_expanded') . '/views_expanded.css');
  }

  // grab entity type from view definition
  $table_data = views_fetch_data($vars['view']->base_table);
  $entity_type = $table_data['table']['entity type'];

  // collect all entity ids
  foreach ($vars['view']->result as $entity) {
    $ids[] = $entity->{$vars['view']->base_field};
  }

  // load & render entities
  $entities = entity_load($entity_type, $ids);
  $entities_view = reset(entity_view($entity_type, $entities, $handler->options['view_mode']));

  foreach ($ids as $i=>$id) {
    $render[$i] = $entities_view[$id];
  }
  $vars['views_expanded_items'] = $render;
}
