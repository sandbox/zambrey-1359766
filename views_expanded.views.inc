<?php

/**
 * @file
 * Defines the View Style Plugins for Views Expanded module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_expanded_views_plugins() {
  return array(
    'module' => 'views_expanded',
    'style' => array(
      'expanded' => array(
        'title' => t('Expanded'),
        'help' => t('Display the results as an expanded menu.'),
        'handler' => 'views_expanded_plugin_style',
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'uses grouping' => FALSE,
        'uses row class' => TRUE,
        'type' => 'normal',
        'theme' => 'views_view_expanded',
        'theme path' => drupal_get_path('module', 'views_expanded') . '/theme',
        'theme file' => 'views_expanded.theme.inc',
      ),
    ),
  );
}
